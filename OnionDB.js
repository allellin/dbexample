/**
 * Created by Fizz on 20/4/15.
 */
(function(global) {

    var extend = function (destination, source) {
        for (var property in source) {
            destination[property] = source[property];
        }
        return destination;
    };

    //define constants
    var ONION_INSERT = 10001,
        ONION_UPDATE = 10002,
        ONION_REMOVE = 10003,
        ONION_MAX = 10004,
        ONION_MIN = 10005,
        ONION_AVERAGE=10006;

    var ONION_INIT = 20001;

    /**
     *  query methods
     *  */

    var queryMethods = {
        insert: function (value) {
            var message = {
                queryType: ONION_INSERT,
                value: value || null
            };
            console.log(message)
            return message;
        },
        update: function (selector, value) {
            var message = {
                queryType: ONION_UPDATE,
                selector: selector || null,
                value: value || null
            };
        },
        remove: function (selector) {
            var message = {
                queryType: ONION_REMOVE,
                selector: selector || null,
                value: value || null
            };
        },
        max: function (selector) {
            var message = {
                queryType: ONION_MAX,
                selector: selector
            };
            return message;
        },
        min: function (selector) {
            var message = {
                queryType: ONION_MIN,
                selector: selector
            };
            return message;
        },
        average: function (selector) {
            var message = {
                queryType: ONION_AVERAGE,
                selector: selector
            };
            return message;
        },
        find: function (selector) {

        }
    };

    function OnionDB(dbName) {

        var scope = this;

        /**
         *  events
         *  */

        var DB_NAME = dbName || null;

        //private eventsList
        var _eventsList = {};

        //API
        this.dispatchEvent = function (eventName) {
            var eventName = eventName.toLowerCase();
            _eventsList[eventName] = [];
        };

        //add event listener for this OnionDB instance with three event types: inserted, updated, removed
        this.on = function (eventName, fn, scope) {
            var eventName = eventName.toLowerCase();
            _eventsList[eventName] = _eventsList[eventName] || [];
            _eventsList[eventName].push({
                fn: fn || null,
                scope: scope || null
            });
        };

        //private
        var fireEvent = function () {
            var args = Array.prototype.slice.call(arguments);
            var eventName = args.shift();
            eventName = eventName.toLowerCase();
            var list = _eventsList[eventName] || [];
            for (var i = 0; i < list.length; i++) {
                var dict = list[i];
                var fn = dict.fn;
                var scope = dict.scope;
                fn.apply(scope || null, args);
            }
        };

        /**
         *  webSocket
         *  */

        //private webSocket
        var _ws = new WebSocket("ws://121.40.121.183:8080");

        //events handler
        var handlers = {
            onopenHandler: function () {
                console.log("open");
                var message = {
                    queryType: ONION_INIT,
                    dbName: DB_NAME
                };
                message = JSON.stringify(message);
                _ws.send(message);
            },
            onmessageHandler: function () {
                var message = arguments[0];
                if (!message)
                    return;
                var data = JSON.parse(message.data || null);
                if (data) {
                    var queryType = parseInt(data.queryType || null);
                    var doc = data.doc || null;
                    switch (queryType) {
                        case ONION_INSERT:
                        {
                            fireEvent("inserted", doc);
                            break;
                        }
                        case ONION_UPDATE:
                        {
                            fireEvent("updated", doc);
                            break;
                        }
                        case ONION_REMOVE:
                        {
                            fireEvent("removed", doc);
                            break;
                        }
                        case ONION_MAX:
                        {
                            fireEvent("maxQueried",doc);
                            break;
                        }
                        case ONION_MIN:
                        {
                            fireEvent("minQueried",doc);
                            break;
                        }
                        case ONION_AVERAGE:
                        {
                            fireEvent("averageQueried",doc);
                            break;
                        }
                    }
                }
            },
            onerrorHandler: function (error) {

            }
        };

        _ws.onopen = handlers.onopenHandler;

        _ws.onerror = function (error) {
            console.log(error);
        };

        _ws.onmessage = handlers.onmessageHandler;

        var checkWsState = function () {
            return _ws.readyState === _ws.OPEN ? true : false;
        };

        this.emit = function () {
            if (!checkWsState()) {
                return;
            }
            var args = Array.prototype.slice.call(arguments);
            var queryMethod = args.shift().toLowerCase();

            if (queryMethods[queryMethod]) {
                var slice = Array.prototype.slice;
                var message = queryMethods[queryMethod].apply(null, slice.call(args));
                message = JSON.stringify(message);
                _ws.send(message);
            }
        };

    }

    global.OnionDB=OnionDB;

}(typeof window !== "undefined" ? window : this));
